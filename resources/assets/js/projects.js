import Vue from 'vue';
import axios from 'axios';
import Form from './core/Form';

window.axios = axios;
window.Form = Form;

new Vue({
    el: '#app',

    data: {
        form: new Form({
            name: '',
            description: '',
            avatar: ''
        })
    },

    methods: {
        previewAvatar(){
            console.log(this.$refs.file.files[0]);

            let reader = new FileReader();
            let vm = this;
            reader.onload = (e) => {
                vm.form.avatar = e.target.result;
            };
            reader.readAsDataURL(this.$refs.file.files[0]);

            //console.log(reader);
        },

        onSubmit() {
            this.form.submit('post', '/projects')
            .then(data => console.log(data))
            .catch(errors => console.log(errors));
        }
    }
});