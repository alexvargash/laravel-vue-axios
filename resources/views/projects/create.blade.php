<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Projects</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
	<style> body{ padding-top: 40px; } </style>
</head>

<body>
	<div id="app" class="container">
		@include('projects.list')

		<form method="POST" action="/projects" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">

			<div class="field">
				<label for="name" class="label">Project Name:</label>
				<div class="control">
					<input type="text" id="name" name="name" class="input" v-model="form.name">
					<span class="help is-danger" v-if="form.errors.has('name')" v-text="form.errors.get('name')"></span>
				</div>
			</div>

			<div class="field">
				<label for="description" class="label">Project Description:</label>
				<div class="control">
					<input type="text" id="description" name="description" class="input" v-model="form.description">
					<span class="help is-danger" v-if="form.errors.has('description')" v-text="form.errors.get('description')"></span>
				</div>
			</div>

            <div class="field">
                <input type="file" ref="file" name="file" v-on:change="previewAvatar">
            </div>

            <div>
                <img :src="form.avatar" v-if="form.avatar">
            </div>

			<div class="field">
				<div class="control">
					<button class="button is-primary" v-bind:disabled="form.errors.any()">Create</button>
				</div>
			</div>
			
		</form>

	</div>

	<!--script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.js"></script-->
    <script src="/js/projects.js?1240"></script>
</body>
</html>